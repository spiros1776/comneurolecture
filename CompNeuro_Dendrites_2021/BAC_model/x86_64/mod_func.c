#include <stdio.h>
#include "hocdec.h"
extern int nrnmpi_myid;
extern int nrn_nobanner_;

extern void _cad2_reg(void);
extern void _childa_reg(void);
extern void _child_reg(void);
extern void _epsp_reg(void);
extern void _it2_reg(void);
extern void _kaprox_reg(void);
extern void _kca_reg(void);
extern void _km_reg(void);
extern void _kv_reg(void);
extern void _na_reg(void);
extern void _SlowCa_reg(void);

void modl_reg(){
  if (!nrn_nobanner_) if (nrnmpi_myid < 1) {
    fprintf(stderr, "Additional mechanisms from files\n");

    fprintf(stderr," \"mechanisms//cad2.mod\"");
    fprintf(stderr," \"mechanisms//childa.mod\"");
    fprintf(stderr," \"mechanisms//child.mod\"");
    fprintf(stderr," \"mechanisms//epsp.mod\"");
    fprintf(stderr," \"mechanisms//it2.mod\"");
    fprintf(stderr," \"mechanisms//kaprox.mod\"");
    fprintf(stderr," \"mechanisms//kca.mod\"");
    fprintf(stderr," \"mechanisms//km.mod\"");
    fprintf(stderr," \"mechanisms//kv.mod\"");
    fprintf(stderr," \"mechanisms//na.mod\"");
    fprintf(stderr," \"mechanisms//SlowCa.mod\"");
    fprintf(stderr, "\n");
  }
  _cad2_reg();
  _childa_reg();
  _child_reg();
  _epsp_reg();
  _it2_reg();
  _kaprox_reg();
  _kca_reg();
  _km_reg();
  _kv_reg();
  _na_reg();
  _SlowCa_reg();
}
