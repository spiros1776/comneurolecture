# Solution
for seg in dend1:
  seg.traub.gNabar = 0.1   # Sodium conductance, S/cm^2
  seg.traub.gKbar = 0.045  # Potassium conductance, S/cm^2
 
actual_epsp = []
N = 10
for i in range(N):
  nc = h.NetCon(ns,Syn, -10, 2, 0.008*(i+1))
 
  h.run()
 
  vsoma_vec.remove(0, 200)
  vdend1_vec.remove(0, 200)
  t_vec.remove(0, 200)
  actual_epsp.append(round(vsoma_vec.max() - vsoma_vec.min(), 2))
 
expected_epsp = [actual_epsp[0]*i for i in range(1, N+1)]
 
print(expected_epsp)
plt.figure(figsize=(10, 6)) 
plt.plot(expected_epsp, actual_epsp, color='red')
plt.plot(expected_epsp, expected_epsp, '--', color='black',
        label='linear summation')
plt.xlabel('Expected EPSP (mV)')
plt.ylabel('Actual EPSP (mV)')
